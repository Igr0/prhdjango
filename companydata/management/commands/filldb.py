from django.core.management.base import BaseCommand, CommandError
from companydata.models import Company
import requests

class Command(BaseCommand):
    help = 'Fills database with companydata.'

    def handle(self, *args, **options):
        req_url = 'https://avoindata.prh.fi/tr/v1'
        payload = {
            'maxResults': '100',
            'companyRegistrationFrom': '2018-01-01',
            'companyRegistrationTo': '2018-05-01',
            'companyForm': 'OY',
        }

        r = requests.get(req_url, params=payload)
        data = r.json()

        for comp in data['results']:
            company = Company(
                businessId = comp['businessId'],
                businessName = comp['name'],
                registrationDate = comp['registrationDate'],
                companyForm = comp['companyForm'],
                detailsUri = comp['detailsUri'],
                bisDetailsUri = comp['bisDetailsUri']
            )
            company.save()

        self.stdout.write(self.style.SUCCESS('Success'))
