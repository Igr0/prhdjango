from django.shortcuts import render, get_object_or_404
from django.views import generic

from .models import Company

class IndexView(generic.ListView):
    template_name = 'companydata/index.html'
    context_object_name = 'companies'

    def get_queryset(self):
        return Company.objects.all()


class DetailView(generic.DetailView):
    model = Company
    template_name = 'companydata/detail.html'
