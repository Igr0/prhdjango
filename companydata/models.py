from django.db import models

class Company(models.Model):
    businessId = models.CharField(max_length=16)
    businessName = models.CharField(max_length=100)
    registrationDate = models.CharField(max_length=16)
    companyForm = models.CharField(max_length=16)
    detailsUri = models.CharField(max_length=200)
    bisDetailsUri = models.CharField(max_length=200)

    def _str__(self):
        return name