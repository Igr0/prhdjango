# PRH Django

### In english
Practice project for reading data from public api and saving it into postgres via Django.


### In finnish/suomeksi
Projektissa luetaan yritysdataa PRH:n apista. Apin palauttama yritysdata tallennetaan postgres kantaan Djangon kautta. Haku toimii komennolla `python manage.py filldb`.

Itse api-kutsu käyttää requests-kirjastoa. Kutsu on tällä hetkellä kovakoodattu(hakee OY:t aikavälille 1.1.18 - 1.5.18). Apihaulla tulee useampi tuhat yritystä, mutta haku palauttaa kerrallaan vain X määrän yrityksiä. Projektin apikutsu hakee 100 ensimmäistä yritystä, jatkossa voisi kokeilla loppujenkin tulosten läpikäyntiä ja tallennusta.
